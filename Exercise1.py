def main():

    numbers = [319231, 65, 191919191, 54]
    result = first_balanced(numbers)
    print("Result is " + str(result))


# This function receives a vector of natural numbers and recursively
# returns first balanced number (if any). If not then return the number
# with greater valencia value

def first_balanced(vec):

    size = len(vec)
    half = int(size/2)

    if size == 1:
        list_of_num = [int(i) for i in str(vec[0])]
        val = abs(valencia(list_of_num))
        return {'num': vec[0], 'val': val}
    else:
        x = first_balanced(vec[0:half])
        y = first_balanced(vec[half:size])
        if x['val'] == 0:
            return x
        elif y['val'] == 0:
            return y
        else:
            return x if x['val'] >= y['val'] else y


# This function receives a vector of natural numbers and recursively
# returns its valencia number.

def valencia(num):

    size = len(num)
    half = int(size/2)

    if size == 1:
        return -num[0]
    elif size == 2:
        return num[1] - num[0]
    else:
        x = valencia(num[half:size])
        y = valencia(num[0:half])
        return x - y


if __name__ == "__main__":
    main()

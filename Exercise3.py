def main():
    print(is_increasing([12341]))


def my_length(L):
    length = 0
    for n in L:
        length += 1
    return length


def my_maximum(L):
    size = len(L)
    half = int(size / 2)
    if size == 1:
        return L[0]
    else:
        x = my_maximum(L[0:half])
        y = my_maximum(L[half:size])
        return max(x, y)


def average(L):
    return summation(L)/len(L)


def summation(L):
    size = len(L)
    half = int(size / 2)
    if size == 1:
        return L[0]
    else:
        x = summation(L[0:half])
        y = summation(L[half:size])
        return x+y


def build_palindrome(L):
    size = len(L)
    palindrome = []
    for i in range(size-1, -1, -1):
        palindrome.append(L[i])
    return palindrome


def remove(L1, L2):
    for item in L1:
        if item in L2:
            L1.remove(item)
    return L1


def flatten(L):
    res = []
    for item in L:
        if isinstance(item, list):
            res += (flatten(item))
        else:
            res.append(item)

    return res


def odds_evens(L):
    odds = []
    evens = []
    for item in L:
        if item%2 == 0:
            evens.append(item)
        else:
            odds.append(item)
    return {'odds': odds, 'evens': evens}


def prime_divisors(num):
    primes = []
    for i in range(2, num, 1):
        if is_prime(i) and num % i == 0:
            primes.append(i)
    return primes


def is_prime(num):
    for n in range(2, num, 1):
        if num % n == 0:
            return False
    return True


def is_increasing(L):
    num_list = []
    for num in L:
        digits_list = [int(i) for i in str(num)]
        num_list.append(is_num_increasing(digits_list))
    return num_list


def is_num_increasing(digits_list):

    size = len(digits_list)
    half = int(size/2)

    if size == 1:
        return True
    elif size == 2:
        return digits_list[0] <= digits_list[1]
    else:
        x = is_num_increasing(digits_list[half:size])
        y = is_num_increasing(digits_list[0:half])
        return x and y


if __name__ == "__main__":
    main()

import sys
import time
from threading import Thread
from PyQt5.QtWidgets import (QWidget, QProgressBar, QPushButton, QApplication)


class Exercise2(QWidget):

    def __init__(self):
        super().__init__()
        self.init_window()

    def init_window(self):

        self.pbar = QProgressBar(self)
        self.pbar.setGeometry(70, 40, 700, 30)

        self.startBtn = QPushButton('Start', self)
        self.startBtn.move(70, 150)
        self.startBtn.clicked.connect(self.start)

        self.stopBtn = QPushButton('Stop', self)
        self.stopBtn.move(610, 150)
        self.stopBtn.clicked.connect(self.stop)

        self.tasks = ExecuteTask(self.pbar)

        self.setGeometry(300, 300, 800, 250)
        self.setWindowTitle('Exercise 2')
        self.show()

    def start(self):
        self.tasks.start()

    def stop(self):
        self.tasks.stop()


class ExecuteTask(Thread):

    def __init__(self, *args):
        Thread.__init__(self)
        self.pb = args[0]
        self.finish = False

    def stop(self):
        self.finish = True

    def run(self):
        step = 0
        tasks = [1, 1, 0.5, 1, 1.5]
        for task in tasks:
            if self.finish:
                break
            time.sleep(task)
            step += 20
            self.pb.setValue(step)

        self.pb.setValue(0)
        print("Thread finished")
        return


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Exercise2()
    sys.exit(app.exec_())